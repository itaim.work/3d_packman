using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private CharacterController controller;
    [SerializeField] private Vector3 move, playerBase;
    [SerializeField] private float speed, inputX, inputY;
    

    private void Start()
    {
        playerBase = transform.position;
        controller = gameObject.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");

        move = new Vector3(inputX, 0, inputY);
        controller.Move(move * speed * Time.deltaTime);
    }

    public void UpdatePos()
    {
        print("asdads");
        controller.enabled = false;
        controller.transform.position  = playerBase;
        controller.enabled = true;
    }
}
