using UnityEngine;
using UnityEngine.AI;

public class AgentController : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;

    [SerializeField] private Transform target;

    [SerializeField] private GameObject[] WayPoints;

    [SerializeField] private GameObject Player;

    [SerializeField] private Vector3 AgentBase, PlayerBase;


    private Vector3 DirectionBetween;

    private RaycastHit hit;

    [SerializeField] private float maxRadius, counter, CounterReset = 5;

    [SerializeField] private bool isInFOV;
    private float maxangle = 360;

    [SerializeField] private GameManager GameManagerScript;


    // Start is called before the first frame update
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();

        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

        Player = GameObject.Find("Player");

        agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);

        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        AgentBase = transform.position;
        PlayerBase = Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.remainingDistance <= 0.2f && !isInFOV)
        {
            agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);
        }

        DirectionBetween = (Player.transform.position - transform.position).normalized;

        if (isInFOV)
        {
            agent.SetDestination(Player.transform.position);
        }

        if (Physics.Raycast(transform.position, DirectionBetween, out hit, maxRadius))
        {
            if (hit.transform.CompareTag("Player"))
            {
                isInFOV = true;
                counter = CounterReset;
            }
        }

        if (counter <= 0 && isInFOV)
        {
            agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);
            isInFOV = false;
        }

        counter -= Time.deltaTime;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManagerScript.DecLifeCounter();
            for (int i = 0; i < GameManagerScript.GetAgents().Length; i++)
            {
                GameManagerScript.GetAgents()[i].GetComponent<AgentController>().ResetPos();
            }
            Player.GetComponent<PlayerController>().UpdatePos();
        }
    }

    public void ResetPos()
    {
        transform.position = AgentBase;
        isInFOV = false;
        agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);

    }

    /// <summary>
    /// visualize the FOV process
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;//A color for the Radius
        Gizmos.DrawWireSphere(transform.position, maxRadius);//Draw the radius with a yellow color 

        Vector3 fovLine1 = Quaternion.AngleAxis(maxangle, transform.up) * transform.forward * maxRadius;//sets the fov aspect, with the radius and the look angel(marked with !)
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxangle, transform.up) * transform.forward * maxRadius;//!

        Gizmos.color = Color.blue;//sets the FOV lines coloer to blue
        Gizmos.DrawRay(transform.position, fovLine1);//draws the FOV aspect on the screen @
        Gizmos.DrawRay(transform.position, fovLine2);//@

        if (!isInFOV)
            Gizmos.color = Color.red;
        if (isInFOV)
            Gizmos.color = Color.green;//the player position ray color
        Gizmos.DrawRay(transform.position, (Player.transform.position - transform.position).normalized * maxRadius);//draws a line to the player position

        Gizmos.color = Color.black;//the forward ray color
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);//draws a ray to the AI forward direction
    }

}
