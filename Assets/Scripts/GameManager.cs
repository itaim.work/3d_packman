using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [SerializeField] private int StarCounter;
    [SerializeField] private int LifeCounter;
    [SerializeField] private GameObject PauseMenu;
    [SerializeField] private GameObject WinMenu;
    [SerializeField] private GameObject LoseMenu;
    [SerializeField] private Text LifeText;
    [SerializeField] private GameObject[] Agents;
    

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        Agents = GameObject.FindGameObjectsWithTag("Agent");
        LifeText = GameObject.Find("Life_Text").GetComponent<Text>();

        StarCounter = GameObject.FindGameObjectsWithTag("Star").Length;
        LifeCounter = 3;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            PauseGame();
        }
        if (StarCounter <= 0)
        {
            WinGame();
        }
        if (LifeCounter <= 0)
        {
            LoseGame();
        }

        LifeText.text = "Life Counter: " + LifeCounter;
    }

    public GameObject[] GetAgents()
    {
        return Agents;
    }

    public void DecStar()
    {
        StarCounter = StarCounter - 1;
    }

    public void DecLifeCounter()
    {
        LifeCounter--;
    }

    public void PauseGame()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void LoseGame()
    {
        LoseMenu.SetActive(true);
        Time.timeScale = 0f;
    }
    public void WinGame()
    {
        WinMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;

        SceneManager.LoadScene(0);
    }

    public void NextLevel()
    {
        Time.timeScale = 1f;

        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        print("Next Level");
    }

}
